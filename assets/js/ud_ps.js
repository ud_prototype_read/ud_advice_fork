var ie=$.browser.msie;
var ie6Flag = ($.browser.msie && $.browser.version.substr(0, 1) < 7) ? true : false;
var ie7Flag = ($.browser.msie && $.browser.version.substr(0, 1) < 8) ? true : false;
var ie8Flag = ($.browser.msie && $.browser.version.substr(0, 1) > 7) ? true : false;
var recpanel = ""; // Global variable to determine which panel is currently used.
/* function to scroll the popup starts*/
$('#contentColumn').scroll(function (e) {
    var topval = $('#contentColumn').scrollTop();
    $('.ui-dialog.ui-widget.ui-widget-content').css('top', -topval);
});
/* function to scroll the popup ends*/
$(window).load(function () {
	/* function to show the popup while loading starts*/
	setTimeout(function () {
    $("#detailspopup").trigger("click");
	},500);
    overlaywidth()
    setTimeout(function () {
        $("#enhanced2_popup").prev().find('a.popup-close').trigger("click");
    }, 60000);
	/* function to show the popup while loading ends*/	
	/*Added for expand/collpase functionality*/	
	setTimeout('expcollapse()', 500);
	
});

/* function to reduce the overlay width to get vertical scroll when modal window opens */
function overlaywidth() {
    var overlaywidth = $(window).width();
    overlaywidth = overlaywidth - 15;
    setTimeout(function () {
        $('.ui-widget-overlay.modal-overlay').css('width', overlaywidth);
    }, 500);
}

$(document).ready(function (e) {
/* generic function for doing expand/collapse action for sublevel panels starts */
    $('a[href=#secondlevel-menu]').on('click', function (e) {
        e.stopImmediatePropagation();
        $(this).ActionLink();
        var val = $(this).attr('dc-name');
        var val0 = "Expand " + val;
        var val1 = "Collapse " + val;
        recpanel = $(this).closest('.hd');
        $('#secondlevel-menu').find('li:eq(0)').find('a').html(val0);
        $('#secondlevel-menu').find('li:eq(1)').find('a').html(val1);


        var currsub = $(this).closest('.hd');
        if (currsub.find('a').hasClass('collapsed')) {
            $('#secondlevel-menu li:eq(0)').removeClass('closed');
            $('#secondlevel-menu li:eq(1)').addClass('closed');
        } else {
            $('#secondlevel-menu li:eq(0)').addClass('closed');
            $('#secondlevel-menu li:eq(1)').removeClass('closed');
        }
        var subexpandedFlag = 0;
        var subexpandedFlagCount = 0;
        var subcollapsedFlagCount = 0;
        currsub.next().find('a.jsExpandCollapse').each(function (index) {
            if ($(this).hasClass('expanded') && !$(this).closest('tr').hasClass('closed')) {
                subexpandedFlag = 1;
                subexpandedFlagCount++;
            } else if ($(this).hasClass('collapsed')) {
                subcollapsedFlagCount++;
            }
        });
        var hiddencount = currsub.next().find('.panels table').find('tr.closed').length;
        var totalcount = currsub.next().find('a.jsExpandCollapse').length - hiddencount;
        if (subexpandedFlag == 1) {
            $('#secondlevel-menu li:eq(2),#secondlevel-menu li:eq(3)').removeClass('closed');
        } else {
            $('#secondlevel-menu li:eq(2)').removeClass('closed');
            $('#secondlevel-menu li:eq(3)').addClass('closed');
        }
        if (totalcount == subexpandedFlagCount) {
            $('#secondlevel-menu li:eq(2)').addClass('closed');
        } else if (totalcount == subexpandedFlagCount) {
            $('#secondlevel-menu li:eq(2)').removeClass('closed');
        }

    })
    /* generic function for doing expand/collapse action for sublevel panels ends */
    /* generic function for doing expand/collapse action using action menu for sublevel panels starts */
    $('#secondlevel-menu li').live('click', function (e) {
        var indx1 = $(this).index();
        if (indx1 == 0) {
            $('#secondlevel-menu li:eq(0)').addClass('closed');
            $('#secondlevel-menu li:eq(1)').removeClass('closed');
            recpanel.trigger('click');
        } else if (indx1 == 1) {
            $('#secondlevel-menu li:eq(0)').removeClass('closed');
            $('#secondlevel-menu li:eq(1)').addClass('closed');
            recpanel.next().find('a.jsExpandCollapse').each(function (index) {
                if ($(this).hasClass('expanded')) {
                    $(this).trigger('click');
                }
            });
            recpanel.trigger('click');
        } else if (indx1 == 2) {
            $('#secondlevel-menu li:eq(2)').addClass('closed');
            $('#secondlevel-menu li:eq(3)').removeClass('closed');
            recpanel.next().find('a.jsExpandCollapse').each(function (index) {
                if ($(this).hasClass('collapsed')) {
                    $(this).trigger('click');
                }
            });
            if (recpanel.find('a').hasClass('collapsed')) {
                recpanel.trigger('click');
            }
        } else if (indx1 == 3) {
            $('#secondlevel-menu li:eq(2)').removeClass('closed');
            $('#secondlevel-menu li:eq(3)').addClass('closed');
            recpanel.next().find('a.jsExpandCollapse').each(function (index) {
                if ($(this).hasClass('expanded')) {
                    $(this).trigger('click');
                }
            });
        }
    });
    /* generic function for doing expand/collapse action using action menu for sublevel panels ends */
    /* function for doing expand/collapse action for portfolio panel starts */
    $('#portfolio_menu').on('click', function (e) {
        e.stopImmediatePropagation();
        $(this).ActionLink();
        var curr = $(this).closest('.hd');
        if (curr.find('a').hasClass('collapsed')) {
            $('#portfolio-menu li:eq(0)').removeClass('closed');
            $('#portfolio-menu li:eq(1)').addClass('closed');
        } else {
            $('#portfolio-menu li:eq(0)').addClass('closed');
            $('#portfolio-menu li:eq(1)').removeClass('closed');
        }
        var expandedFlag = 0;
        var expandedFlagCount = 0;
        var collapsedFlagCount = 0;
        curr.next().find('.hd').each(function (index) {
            if ($(this).find('a').hasClass('expanded')) {
                expandedFlag = 1;
                expandedFlagCount++;
            } else {
                collapsedFlagCount++;
            }
        });
        if (expandedFlag == 1) {
            $('#portfolio-menu li:eq(2),#portfolio-menu li:eq(3)').removeClass('closed');
        } else {
            $('#portfolio-menu li:eq(2)').removeClass('closed');
            $('#portfolio-menu li:eq(3)').addClass('closed');
        }
        if (curr.next().find('.hd').length == expandedFlagCount) {
            $('#portfolio-menu li:eq(2)').addClass('closed');
        } else if (curr.find('.hd').length == collapsedFlagCount) {
            $('#portfolio-menu li:eq(2)').removeClass('closed');
        }
    });
    /* function for doing expand/collapse action for portfolio panel ends */
    /* function for doing expand/collapse action for portfolio panel using actions menu starts */
    $('#portfolio-menu li').live('click', function (e) {
        var indx = $(this).index();
        if (indx == 0) {
            $('#portfolio-menu li:eq(0)').addClass('closed');
            $('#portfolio-menu li:eq(1)').removeClass('closed');
            $('#Portfolio').trigger('click');
        } else if (indx == 1) {
            $('#portfolio-menu li:eq(0)').removeClass('closed');
            $('#portfolio-menu li:eq(1)').addClass('closed');
            $('#Portfolio').trigger('click');
            $('#Portfolio').next().find('.hd').each(function (index) {
                if ($(this).find('a').hasClass('expanded')) {
                    $(this).trigger('click');
                }
            });
        } else if (indx == 2) {
            $('#portfolio-menu li:eq(2)').addClass('closed');
            $('#portfolio-menu li:eq(3)').removeClass('closed');
            $('#Portfolio').next().find('.hd').each(function (index) {
                if ($(this).find('a').hasClass('collapsed')) {
                    $(this).trigger('click');
                }
            });
            if ($('#Portfolio').find('a').hasClass('collapsed')) {
                $('#Portfolio').trigger('click');
            }
        } else if (indx == 3) {
            $('#portfolio-menu li:eq(2)').removeClass('closed');
            $('#portfolio-menu li:eq(3)').addClass('closed');
            $('#Portfolio').next().find('.hd').each(function (index) {
                if ($(this).find('a').hasClass('expanded')) {
                    $(this).trigger('click')
                }
            });

        }
    });
    /* function for doing expand/collapse action for portfolio panel using actions menu ends */

/* function for show zero balance starts*/ // by default closing all the rows 
$('tr.hidezerobal').prev().addClass('closed').removeClass('activated'); //by default closing the previous row for plan panels alone
$('tr.hidezerobal').prev().find('a.jsExpandCollapse').removeClass('collapsed expanded jsExpandCollapse').addClass('zerobal'); /* by default removing the class for expand/collapse functionality*/
$('#portfolio_panel input[type=checkbox]').bind('click change', function () {
/*Functionality to show/hide the table contents based on the checkbox (check/uncheck) starts*/																  
			if ($(this).attr('checked')) {
                        var arr = $(this).parent().parent().next().find('tr.zerobalcontent');
                        arr.each(function (e) {
							$(this).prev().removeClass('closed');
							if($(this).hasClass('detail activated')) {
								$(this).prev().find('a.zerobal').addClass('expanded jsExpandCollapse');
							}
							else if($(this).hasClass('detail')) {
								$(this).prev().find('a.zerobal').addClass('collapsed jsExpandCollapse');
							}					
							
							$(this).removeClass('hidezerobal');
							if($(this).prev().find('a.jsExpandCollapse').hasClass('collapsed')) {			   
								$(this).css('display','none');
								$('tr.zerobalcontent').prev().removeClass('activated');
							}
							else if($(this).prev().find('a.jsExpandCollapse').hasClass('expanded')) {
								if(ie6Flag || ie7Flag)	{
									$(this).css('display','block');									
								}
								else {
									$(this).css('display','table-row');										
								}
							}
													
                        });

                    } else {
                        var arr = $(this).parent().parent().next().find('tr.zerobalcontent');						
						arr.addClass('hidezerobal');
						arr.prev().find('a.zerobal').removeClass('expanded collapsed jsExpandCollapse');
                        arr.each(function (e) {                            
							$(this).css('display','none');
							$(this).prev().addClass('closed');
                        });
                     }
/*Functionality to show/hide the table contents based on the checkbox (check/uncheck) ends*/
/*Functionality to check/uncheck the 'show zero balance across all products' checkbox if all the other checkbox is checked or if anyone uncheck starts*/
			var chk = $('#portfolio_panel').find('input[type=checkbox]');
			var chklength = chk.length;
			var flagchecked1 = 0;
			var flagunchecked1 = 0;
			chk.each(function (index) {
				if ($(this).attr('checked')) {
					flagchecked1++;
				} else {
					flagunchecked1++;
				}
			});

			if (chklength == flagchecked1) {
				$('#checkbox_1').attr('checked', 'checked')
			} else if (chklength != flagunchecked1) {
				$('#checkbox_1').removeAttr('checked');
			}
/*Functionality to check/uncheck the 'show zero balance across all products' checkbox if all the other checkbox is checked or if anyone uncheck ends*/
});
/*Functionality for show zero balance across all products (i.e check all/ uncheck all) starts*/	
$('#checkbox_1').live('click', function (e) {
	if ($(this).attr('checked')) {
		$('#portfolio_panel input[type=checkbox]').each(function (e) {
			if (!$(this).attr('checked')) {
				$(this).trigger('click').trigger('change');
			}
		});
	} else {
		$('#portfolio_panel input[type=checkbox]').each(function (e) {
			if ($(this).attr('checked')) {
				$(this).trigger('click').trigger('change');

			}
		});
	}
});
/*Functionality for show zero balance across all products (i.e check all/ uncheck all) ends*/
/* function for show zero balance ends*/

/*function to Automatically expand sections when Table of Contents Links are selected on the Participant Summary page starts*/
	$("ul.leftul li a").live('click', function (e) {	
		var targetid = $(this).attr('href');
		var status = $(targetid).find('a');
		if(status.hasClass('collapsed'))
		{
			$(targetid).trigger('click');
			
		}
	});

/*function to Automatically expand sections when Table of Contents Links are selected on the Participant Summary page ends*/   

    /* function for table sorting, pagination and filtering starts */
	if($('#customerRequest').length>0)
	{
		var tbl_persons__js = new tiaacref.tableSort({
		id: '#customerRequest',
		multiSort: false,
		onBeforeSort: function(e){''},
		onAfterSort: function(e){''},
		caseSensitive: false,
		initialSort: ''
		});
		tbl_persons__js.sort( 'firstcolumn', 'desc', false );
	}
    if ($('#tbl_persons0_auth').length > 0) {
        var tbl_persons__js = new tiaacref.tableSort({
            id: '#tbl_persons0_auth',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });
        tbl_persons__js.sort('expdate', 'desc', false);
    }
    if ($('#tbl_persons0').length > 0) {
        var tbl_persons__js = new tiaacref.tableSort({
            id: '#tbl_persons0',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });
        tbl_persons__js.sort('firstName', 'desc', false);
    }

    if ($('#tbl_persons1').length > 0) {
        var tbl_persons__js = new tiaacref.tableSort({
            id: '#tbl_persons1',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });
        tbl_persons__js.sort('type', 'desc', false);
    }

    if ($('#beneficiaries').length > 0) {
        var tbl_persons__js = new tiaacref.tableSort({
            id: '#beneficiaries',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });
        tbl_persons__js.sort('firstName', 'desc', false);
    }

    if ($('#retirementAccounts').length > 0) {
        var tbl_persons__js = new tiaacref.tableSort({
            id: '#retirementAccounts',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });
        tbl_persons__js.sort('firstName', 'desc', false);
    }

    if ($('#insuranceacct').length > 0) {
        var tbl_persons__js = new tiaacref.tableSort({
            id: '#insuranceacct',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });
        tbl_persons__js.sort('acct', 'desc', false);
    }

    if ($('#brokerageAccounts').length > 0) {
        var tbl_persons__js = new tiaacref.tableSort({
            id: '#brokerageAccounts',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });
        tbl_persons__js.sort('firstName', 'desc', false);
    }

    if ($('#bankingAccounts').length > 0) {
        var tbl_persons__js = new tiaacref.tableSort({
            id: '#bankingAccounts',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });
        tbl_persons__js.sort('firstName', 'desc', false);
    }

    if ($('#clientAlerts').length > 0) {
        var tbl_persons__js = new tiaacref.tableSort({
            id: '#clientAlerts',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });
        tbl_persons__js.sort('edate', 'desc', false);
    }

    if ($('#planAlerts').length > 0) {
        var tbl_persons__js = new tiaacref.tableSort({
            id: '#planAlerts',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });
        tbl_persons__js.sort('edate', 'desc', false);
    }

    if ($('#employeeAlerts').length > 0) {
        var tbl_persons__js = new tiaacref.tableSort({
            id: '#employeeAlerts',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });
        tbl_persons__js.sort('effectivedate', 'desc', false);
    }

    if ($('#locationAlerts').length > 0) {
        var tbl_persons__js = new tiaacref.tableSort({
            id: '#locationAlerts',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });
        tbl_persons__js.sort('effectivedate', 'desc', false);
    }


    if ($('#authSigners').length > 0) {
        var tbl_persons__js = new tiaacref.tableSort({
            id: '#authSigners',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });
        tbl_persons__js.sort('firstName', 'desc', false);
    }

    if ($('#recentActivity').length > 0) {
        var tbl_persons__js = new tiaacref.tableSort({
            id: '#recentActivity',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });
        tbl_persons__js.sort('firstName', 'desc', false);
    }

    if ($('#pendingTransaction').length > 0) {
        var tbl_persons__js = new tiaacref.tableSort({
            id: '#pendingTransaction',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });
        tbl_persons__js.sort('receiveddate', 'desc', false);
    }

    if ($('#serviceTask').length > 0) {
        var tbl_persons__js = new tiaacref.tableSort({
            id: '#serviceTask',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });
        tbl_persons__js.sort('taskid', 'desc', false);
    }

    if ($('#recentHistory').length > 0) {
        var tbl_persons__js = new tiaacref.tableSort({
            id: '#recentHistory',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });
        tbl_persons__js.sort('firstName', 'desc', true);
        new tiaacref.paginate({
            id: '#tcId3',
            table: '#recentHistory',
            perPage: 3,
            startPage: 1,
            autoCount: true,
            multiSort: false,
            showPerPageDropDown: false,
            dataSource: '',
            pageListingLimit: null
        });
    }

    if ($('#mutualFunds').length > 0) {
        var tbl_persons__js = new tiaacref.tableSort({
            id: '#mutualFunds',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });
        tbl_persons__js.sort('firstName', 'desc', true);
        new tiaacref.paginate({
            id: '#tcId4',
            table: '#mutualFunds',
            perPage: 3,
            startPage: 1,
            autoCount: true,
            multiSort: false,
            showPerPageDropDown: false,
            dataSource: '',
            pageListingLimit: null
        });
    }

    if ($('#contactAlerts').length > 0) {
        var tbl_persons__js = new tiaacref.tableSort({
            id: '#contactAlerts',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });
        tbl_persons__js.sort('effectivedate', 'desc', true);
        /*new tiaacref.paginate({
            id: '#tcId5',
            table: '#contactAlerts',
            perPage: 3,
            startPage: 1,
            autoCount: true,
            multiSort: false,
            showPerPageDropDown: false,
            dataSource: '',
            pageListingLimit: null
        });*/
    }

    if ($('#immediateannuity').length > 0) {
        var tbl_persons__js = new tiaacref.tableSort({
            id: '#immediateannuity',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });
        tbl_persons__js.sort('acct', 'desc', true);
    }

    if ($('#propacct').length > 0) {
        var tbl_persons__js = new tiaacref.tableSort({
            id: '#propacct',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });
        tbl_persons__js.sort('acct', 'desc', true);
    }
    if ($('#opportunitiestable').length > 0) {
        var tbl_persons__js = new tiaacref.tableSort({
            id: '#opportunitiestable',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });
        tbl_persons__js.sort('dateCreated', 'desc', true);
    }

    /*Added for recent contact history*/

    if ($('#recent_interactions').length > 0) {

        var recent_interactions_js = new tiaacref.tableSort({
            id: '#recent_interactions',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });

        recent_interactions_js.sort('date', 'desc', true);


    }


    if ($('#recent_activities').length > 0) {

        var recent_activities_js = new tiaacref.tableSort({
            id: '#recent_activities',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });

        recent_activities_js.sort('datecreated', 'desc', true);


    }

    if ($('#recent_opportunities').length > 0) {
        var recent_opportunities_js = new tiaacref.tableSort({
            id: '#recent_opportunities',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });
        recent_opportunities_js.sort('startdate', 'desc', true);
    }

    if ($('#recent_service').length > 0) {

        var recent_service_js = new tiaacref.tableSort({
            id: '#recent_service',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });

        recent_service_js.sort('datecreated', 'desc', true);


    }

    if ($('#recent_fullfillments').length > 0) {

        var recent_fullfillments_js = new tiaacref.tableSort({
            id: '#recent_fullfillments',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });

        recent_fullfillments_js.sort('datecreated', 'desc', true);


    }
    /*Added for customer request table*/
    if ($('#customerrequest').length > 0) {

        var customerrequest_js = new tiaacref.tableSort({
            id: '#customerrequest',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });

        customerrequest_js.sort('recdate', 'desc', true);


    }
    /*added for rtd/pagination table*/
    if ($('#rtdpartimsg').length > 0) {
        var tbl_rtdpartimsg_js = new tiaacref.tableSort({
            id: '#rtdpartimsg',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });
        tbl_rtdpartimsg_js.sort('cdates', 'desc', true);
    }

    /*added for ps_enhanced_05.html - opportunities table in Recent contact history updates*/
    if ($('#recent_opportunities_05').length > 0) {
        var recent_opportunities5_js = new tiaacref.tableSort({
            id: '#recent_opportunities_05',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });
        recent_opportunities5_js.sort('startdate', 'desc', true);
    }
 /*Added for authorized parties with pagination*/
   if ($('#tbl_persons1lat').length > 0) {
        var tbl_persons__js = new tiaacref.tableSort({
            id: '#tbl_persons1lat',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });
        tbl_persons__js.sort('type', 'desc', true);
        new tiaacref.paginate({
            id: '#authpag',
            table: '#tbl_persons1lat',
            perPage: 2,
            startPage: 1,
            autoCount: true,
            multiSort: false,
            showPerPageDropDown: false,
            dataSource: '',
            pageListingLimit: null
        });
    }
/*Added for Coverage Team with pagination*/

if ($('#tbl_persons_coverage').length > 0) {
        var tbl_persons_coverage_js = new tiaacref.tableSort({
            id: '#tbl_persons_coverage',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });
        tbl_persons_coverage_js.sort('firstName', 'desc', true);
        new tiaacref.paginate({
            id: '#coveragepag',
            table: '#tbl_persons_coverage',
            perPage: 2,
            startPage: 1,
            autoCount: true,
            multiSort: false,
            showPerPageDropDown: false,
            dataSource: '',
            pageListingLimit: null
        });
    }
	
/*Added for Beneficiaries with pagination*/	
	if ($('#beneficiaries_pag').length > 0) {
        var tbl_persons__js = new tiaacref.tableSort({
            id: '#beneficiaries_pag',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });
        tbl_persons__js.sort('firstName', 'desc', false);
		new tiaacref.paginate({
            id: '#benepag',
            table: '#beneficiaries_pag',
            perPage: 2,
            startPage: 1,
            autoCount: true,
            multiSort: false,
            showPerPageDropDown: false,
            dataSource: '',
            pageListingLimit: null
        });
    }
	
/*Added for Recent contact history pagination starts*/
/*opportunity table*/
if ($('#recent_opportunities_ias_02').length > 0) {
        var recent_opportunities5_js = new tiaacref.tableSort({
            id: '#recent_opportunities_ias_02',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });
        recent_opportunities5_js.sort('startdate', 'desc', true);
		new tiaacref.paginate({
            id: '#opp_pagn_ias_02',
            table: '#recent_opportunities_ias_02',
            perPage: 2,
            startPage: 1,
            autoCount: true,
            multiSort: false,
            showPerPageDropDown: false,
            dataSource: '',
            pageListingLimit: null
        });
    }
	
/*Service Requests table*/	
if ($('#recent_service_ias_02').length > 0) {

        var recent_service_js = new tiaacref.tableSort({
            id: '#recent_service_ias_02',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });

        recent_service_js.sort('datecreated', 'desc', true);
		new tiaacref.paginate({
            id: '#serv_pagn_ias_02',
            table: '#recent_service_ias_02',
            perPage: 1,
            startPage: 1,
            autoCount: true,
            multiSort: false,
            showPerPageDropDown: false,
            dataSource: '',
            pageListingLimit: null
        });

    }
/*Fullfilments table*/
 if ($('#recent_fullfillments_ias_02').length > 0) {

        var recent_fullfillments_js = new tiaacref.tableSort({
            id: '#recent_fullfillments_ias_02',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });

        recent_fullfillments_js.sort('datecreated', 'desc', true);
		new tiaacref.paginate({
            id: '#fulfilmnt_pagn_ias_02',
            table: '#recent_fullfillments_ias_02',
            perPage: 2,
            startPage: 1,
            autoCount: true,
            multiSort: false,
            showPerPageDropDown: false,
            dataSource: '',
            pageListingLimit: null
        });

    }
	/*Added for Recent contact history pagination ends*/
	
/*Added for Individual Client Communications table*/
 if ($('#individualclient').length > 0) {

        var individualclient_js = new tiaacref.tableSort({
            id: '#individualclient',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });

        individualclient_js.sort('datecreated', 'desc', true);		

}

/*Added for ir_aug_05.html starts*/

if ($('#vintage').length > 0) {

        var vintage_js = new tiaacref.tableSort({
            id: '#vintage',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });

        vintage_js.sort('timeperiod', 'desc', true);		

}

if ($('#vintage1').length > 0) {

        var vintage1_js = new tiaacref.tableSort({
            id: '#vintage1',
            multiSort: false,
            onBeforeSort: function (e) {
                ''
            },
            onAfterSort: function (e) {
                ''
            },
            caseSensitive: false,
            initialSort: ''
        });

        vintage1_js.sort('timeperiod', 'desc', true);		

}
/*Added for ir_aug_05.html ends*/

/*added for advice details page starts*/          
if($('#opp_last_soln').length>0)
{
                var tbl_opp_last_soln_js = new tiaacref.tableSort({
                                id: '#opp_last_soln',
                                multiSort: false,
                                onBeforeSort: function(e){''},
                                onAfterSort: function(e){''},
                                caseSensitive: false,
                                initialSort: ''
                });
                tbl_opp_last_soln_js.sort( 'parti', 'desc', true );
}
if($('#soln_ann_review').length>0)
{
                var tbl_soln_ann_js = new tiaacref.tableSort({
                                id: '#soln_ann_review',
                                multiSort: false,
                                onBeforeSort: function(e){''},
                                onAfterSort: function(e){''},
                                caseSensitive: false,
                                initialSort: ''
                });
                tbl_soln_ann_js.sort( 'parti', 'desc', true );
}
if($('#advice_history').length>0)
{
                var tbl_soln_ann_js = new tiaacref.tableSort({
                                id: '#advice_history',
                                multiSort: false,
                                onBeforeSort: function(e){''},
                                onAfterSort: function(e){''},
                                caseSensitive: false,
                                initialSort: ''
                });
                tbl_soln_ann_js.sort( 'date', 'desc', true );
}
/*added for advice details page ends*/

/*Added for advisor details*/
$('#viewfulldetails').on('click', function (e) {
        e.stopImmediatePropagation();
		window.location="ud_adv.html";
});
});
/* function for table sorting, pagination and filtering ends */
function expcollapse(){
	/* function for doing expand/collapse action for portfolio panel using expand/collapse link starts */
    var portfoliosep = $('#portfolio_menu_collapse').closest('li');
    /*function for doing expand/collapse action for portfolio panel starts*/
    $('#Portfolio_ps').on('click', function (e) {		 
        var flagcollapsedmain = 0;
        var arr1 = $('#portfolio_content .hd').closest('.content').find('.hd');
        var arrlength1 = arr1.length;

        if ($(this).hasClass('hdexpanded')) {
            $('#portfolio_menu_expand').removeClass('screenReader');
            $('#portfolio_content .hd').each(function (index) {
                if (!$(this).hasClass('hdexpanded')) {
                    flagcollapsedmain++;
                }
            });

            if (arrlength1 == flagcollapsedmain) {
                $('#portfolio_menu_collapse').addClass('screenReader');
                portfoliosep.addClass('screenReader');
            } else {
                $('#portfolio_menu_collapse').removeClass('screenReader');
                portfoliosep.removeClass('screenReader');
            }
        } else {
            $('#portfolio_menu_expand').addClass('screenReader');
            $('#portfolio_menu_collapse').addClass('screenReader');
            portfoliosep.addClass('screenReader');
        }
    });
    /*function for doing expand/collapse action for portfolio panel ends*/

  /*function for doing expand(2 levels of expand) for portfoilo panel expand link starts*/
    $('#portfolio_menu_expand').on('click', function (e) {
        e.stopPropagation();
        var arr1 = $('#portfolio_content').find('.hd');
        var arrlength1 = arr1.length;
        var flagexpanded1 = 0;
        arr1.each(function (index) {
            if ($(this).hasClass('hdexpanded')) {
                flagexpanded1++;
            }
        });
		
        if (arrlength1 == flagexpanded1) {
            var arr3 = $('#portfolio_content').find('a.jsExpandCollapse');
            var arrlength3 = arr3.length;
            var flagexpanded3 = 0;
            arr3.each(function (index) {
                if (!$(this).hasClass('expanded')) {
                    $(this).trigger('click');
					$('#portfolio_menu_expand').addClass('disabled');
                }
            });
        } else {
            arr1.each(function (index) {
                if (!$(this).hasClass('hdexpanded')) {
                    $(this).trigger('click');
                }
            });
        }
    });

    /*function for doing expand(2 levels of expand) for portfoilo panel expand link ends*/

    /*function for doing collapse(2 levels of collapse) for collapse link beside portfolio panel starts*/
    $('#portfolio_menu_collapse').on('click', function (e) {
        e.stopPropagation();
        var arr1 = $('#portfolio_content').find('a.jsExpandCollapse');
        var arrlength1 = arr1.length;
        var flagexpanded1 = 0;
        arr1.each(function (index) {
            if ($(this).hasClass('collapsed')) {
                flagexpanded1++;
            }
        });
        if (arrlength1 == flagexpanded1) {
            var arr3 = $('#portfolio_content').find('.hd');
            var arrlength3 = arr3.length;
            var flagexpanded3 = 0;
            arr3.each(function (index) {
                if ($(this).hasClass('hdexpanded')) {
                    $(this).trigger('click');
                }
            });

        } else {
            arr1.each(function (index) {
                if ($(this).hasClass('expanded')) {
                    $(this).trigger('click');
					$('#portfolio_menu_expand').removeClass('disabled');
                }
            });
        }
    });
    /*function for doing collapse(2 levels of collapse) for collapse link beside portfolio panel ends*/

    /*function to show/hide the expand/collapse link beside second level panel based on the second level panel expanded/collapsed state starts*/
    $('#portfolio_content a.expcolbutton').closest('.hd').on('click', function (e) {
        if ($(this).hasClass('hdexpanded')) {
            $(this).find('a.expcolbutton').removeClass('screenReader');
        } else {
            $(this).find('a.expcolbutton').addClass('screenReader');
        }

    });
    /*function to show/hide the expand/collapse link beside second level panel based on the second level panel expanded/collapsed state ends*/

/*function to change the expand/collapse link text beside second level panel based on the fourth level accordian(Nested tables) starts*/
    $('#portfolio_content a.jsExpandCollapse').live('click', function (e) {
/*Functionality to change the text to expand/collapse at the second level panel based on the fourth level accordian(Nested tables) expanded/collapsed state starts*/
        var arr = $(this).closest('.content').find('a.jsExpandCollapse');
        var arrlength = arr.length;
		var flagexpanded = 0;
        var flagcollapsed = 0;
		arr.each(function (index) {
            if ($(this).hasClass('expanded') && !($(this).closest('tr').next().hasClass('hidezerobal'))) {				
                flagexpanded++;
            } else if ($(this).hasClass('collapsed') && !($(this).closest('tr').next().hasClass('hidezerobal'))) {
                flagcollapsed++;
            }		
        });	
        if (arrlength == flagexpanded) {
            $(this).closest('.content').prev().find('a.expcolbutton').html('Collapse');
        } else if (arrlength == flagcollapsed) {
            $(this).closest('.content').prev().find('a.expcolbutton').html('Expand');
        }
/*Functionality to change the text to expand/collapse at the second level panel based on the last level panel expanded/collapsed state ends*/		
/*Functionality to change the expand link to disable at the portfolio main level based on the fourth level accordian(Nested tables) expanded/collapsed state starts*/
		var arr1 = $('#portfolio_content').find('.hd');
        var arrlength1 = arr1.length;
		var commonarr=$('#portfolio_content').find('a.jsExpandCollapse');
		var commonarrlength=commonarr.length;
        var flagexpanded1 = 0;
		var flagexpandedarr = 0;
        arr1.each(function (index) {
            if ($(this).hasClass('hdexpanded')) {
                flagexpanded1++;
            }
        });
		 if (arrlength1 == flagexpanded1) {
				commonarr.each(function (index) {
					if ($(this).hasClass('expanded')) {
						flagexpandedarr++;
					} else if ($(this).hasClass('collapsed')) {
						$('#portfolio_menu_expand').removeClass('disabled');
					}
				});		
				if (commonarrlength == flagexpandedarr) {
					$('#portfolio_menu_expand').addClass('disabled');			
				}
		 }
/*Functionality to change the expand link at the portfolio main level to disable based on the last level panel expanded/collapsed state ends*/
});
/*function to change the expand/collapse link text beside second level panel based on the fourth level accordian(Nested tables) ends*/
/*function to remove the collapse link at the main portfoio panel based on the second level panel starts*/
    $('#portfolio_content .hd').on('click', function (e) {        
        var arr1 = $(this).closest('.content').find('.hd');
        var arrlength1 = arr1.length;
        var flagexpanded1 = 0;
        var flagcollapsed1 = 0;
        arr1.each(function (index) {
            if ($(this).hasClass('hdexpanded')) {
                $('#portfolio_menu_collapse').removeClass('screenReader');
                portfoliosep.removeClass('screenReader');
                flagexpanded1++;
            } else if (!$(this).hasClass('hdexpanded')) {
                flagcollapsed1++;
				$('#portfolio_menu_expand').removeClass('disabled');
            }
        });

        if (arrlength1 == flagexpanded1) {
            $('#portfolio_menu_collapse').removeClass('screenReader');
            portfoliosep.removeClass('screenReader');
			var arr3 = $('#portfolio_content').find('a.jsExpandCollapse');
            var arrlength3 = arr3.length;
            var flagexpanded3 = 0;
                arr3.each(function (index) {
                    if ($(this).hasClass('expanded')) {
						flagexpanded3++;
                    }
				});
			
			    if (arrlength3 == flagexpanded3) {
					$('#portfolio_menu_expand').addClass('disabled');
				}
				else {
					$('#portfolio_menu_expand').removeClass('disabled');
				}
			
			
        } else if (arrlength1 == flagcollapsed1) {
            $('#portfolio_menu_collapse').addClass('screenReader');
            portfoliosep.addClass('screenReader');
        }

    });
    /*function to remove the collapse link at the main portfoio panel based on the second level panel ends*/

    /*function for doing expand/collpase logic based on the link beside second level panel starts*/
    $('#portfolio_content a.expcolbutton').on('click', function (e) {
        var subpanel = $(this).closest('.hd');
        if (subpanel.hasClass('hdexpanded')) {
            e.stopImmediatePropagation();
        }
        var subbutton = $(this);
        if ($(this).html() == "Expand") {
			
            subpanel.next().find('a.jsExpandCollapse').each(function (index) {
                if ($(this).hasClass('collapsed')) {
                    $(this).trigger('click');
                    subbutton.html('Collapse');
                }
            });
        } else if ($(this).html() == "Collapse") {
            subpanel.next().find('a.jsExpandCollapse').each(function (index) {
                if ($(this).hasClass('expanded')) {
                    $(this).trigger('click');
                    subbutton.html('Expand');
					$('#portfolio_menu_expand').removeClass('disabled');
                }
            });
        }
    });
    /*function for doing expand/collpase logic based on the link beside second level panel ends*/
/* function for doing expand/collapse action for portfolio panel using expand/collapse link ends */
}